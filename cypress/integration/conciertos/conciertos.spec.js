describe('Pruebas de la página de conciertos', () => {

  it('Que haya al menos 3 conciertos en la ventana', () => {
    cy.visit('https://curricewebsite.wordpress.com/fecha-proximos-conciertos/');
    cy.get('.wp-block-image').its('length').should('be.gte', 3);
  });
})

