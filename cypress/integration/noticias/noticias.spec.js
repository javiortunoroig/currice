describe('Pruebas de la página de conciertos', () => {

  it('Que haya al menos 3 conciertos en la ventana', () => {
    cy.visit('https://curricewebsite.wordpress.com/noticias/');
    cy.get('.wp-block-media-text').its('length').should('be.gte', 4);
  });
})