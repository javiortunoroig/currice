describe('', (  ) => {

  beforeEach(() => {
    cy.visit('https://curricewebsite.wordpress.com/quienes-somos/')
  });

  it('Accede al trabajado Cristhian', (  ) => {
    cy.contains('Cristhian').click();
    cy.contains('Cristhian Velasco López').should('to.be.visible');
    cy.contains('5bzvelasco@gmail.com').should('to.be.visible');
    cy.contains('Pruebas').should('to.be.visible');
  });
  
  it('Accede al trabajado Javier', (  ) => {
    cy.contains('Javier').click();
    cy.contains('Javier Ortuño Roig').should('to.be.visible');
    cy.contains('javi_roig@hotmail.es').should('to.be.visible');
    cy.contains('Líder técnico').should('to.be.visible');
    cy.contains('Responsable de Gestión de la Configuración').should('to.be.visible');
  });
  
  it('Accede al trabajado Ismael', (  ) => {
    cy.contains('Ismael').click();
    cy.contains('Ismael Toré Franco').should('to.be.visible');
    cy.contains('ismaeltf17.ins@gmail.com').should('to.be.visible');
    cy.contains('diseño').should('to.be.visible');
  });
  
  it('Accede al trabajado Alejandro', (  ) => {
    cy.contains('Alejandro').click();
    cy.contains('Alejandro Ramet Espinosa').should('to.be.visible');
    cy.contains('alexrametespinosa@uma.es').should('to.be.visible');
    cy.contains('Responsable de Requisitos').should('to.be.visible');
    cy.contains('Responsable del Cliente').should('to.be.visible');
  });
  
  it('Accede al trabajado David', (  ) => {
    cy.contains('David').click();
    cy.contains('David Castillo Berná').should('to.be.visible');
    cy.contains('davidcastillo200.dc@gmail.com').should('to.be.visible');
    cy.contains('Jefe de').should('to.be.visible');
    cy.contains('Responsable de Recursos Humanos').should('to.be.visible');
  });
})