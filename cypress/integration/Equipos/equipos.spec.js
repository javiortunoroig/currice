describe('Entrar página equipo', () => {
  it('Entrar en página equipos', () => {

    cy.visit('https://curricewebsite.wordpress.com/');
    cy.contains('Equipo').click();
    cy.url().should('include', '/quienes-somos/');

  });
})


describe('Pruebas de la página de equipos', () => {

  it('Tienen que estar todos los integrantes del grupo', (  ) => {

    cy.contains('Javier').should('to.contain', 'Javier Ortuño');
    cy.contains('Alejandro').should('to.contain', 'Alejandro Ramet');
    cy.contains('Ismael').should('to.contain', 'Ismael Tore');
    cy.contains('Cristhian').should('to.contain', 'Cristhian Velasco');
    cy.contains('David').should('to.contain', 'David Castillo');
    
  });

});