describe('Pruebas de la ventana de home', () => {

  it('Comprobar que aparecen todas las opciones del menú', () => {
    cy.visit('https://curricewebsite.wordpress.com/');
    cy.contains('Home').should('be.visible');
    cy.contains('Biografía').should('be.visible');
    cy.contains('Obras del autor').should('be.visible');
    cy.contains('Próximos conciertos').should('be.visible');
    cy.contains('Noticias').should('be.visible');
    cy.contains('Equipo').should('be.visible');
  });

  it('Comprobar que hay 3 quotes', () => {
    cy.get('.wp-block-columns').children().should('have.length', 3);
  })
})

