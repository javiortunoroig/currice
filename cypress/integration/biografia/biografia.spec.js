 describe('Pruebas de la ventana de home', () => {

  it('Estan todas las secciones', () => {
    cy.visit('https://curricewebsite.wordpress.com/biografia');
    cy.contains('Biografía').should('be.visible');
    cy.contains('Música').should('be.visible');
    cy.contains('Youtube').should('be.visible');
  });
})

