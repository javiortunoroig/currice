<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dbcurrice' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';pI0W1V>ekz&*)b+zyuXyPlrqNn2:;Wm2JZuC)#F17XRi4W1(!EPBg!Yce^@:v_O' );
define( 'SECURE_AUTH_KEY',  '<S:/,y>C++FP2ca6l]t~/zA[RSVCU<*V]ai@HC@7^:JH|pQIILL,Wr<<]7l^ b q' );
define( 'LOGGED_IN_KEY',    'vk6l]=BcPw6)7Ra`CImhT mOy)(frg)Fi8;3F.B$6^Rc8{xEIkx9ZeW;WXo3s}Hi' );
define( 'NONCE_KEY',        '1O@vso#RhgU}2X9,46PT|]ji^6H:Gic-S.Sk%P7ILYX*R~bGh^0g!8D&EOawWjpE' );
define( 'AUTH_SALT',        '90R6@h]~?8F ` z`J3;Y^f#;`*=zL6h*<jJ>7Y_Zhlr*-;);|q3>T{2<W=1!rrX6' );
define( 'SECURE_AUTH_SALT', 't;v]4pIz:;$.2Ek.qJ?yFnM[X(OF1]:7+C8ahqPD(b}WC)C?t9XA(=.(Igs}mKx4' );
define( 'LOGGED_IN_SALT',   'h|fAsNUO?bf~&lOMQQ+8 UdclTs,E;/*MZKm;b_x)CA;%,-d/@1o5a1KpZ[oWP :' );
define( 'NONCE_SALT',       'xPZl7O$DzsuD<Yi0n&@1A&.AYB;#+J?eSp$ByJ`**DvZ*/P_VSJ`(4:,OD5{.W;N' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
