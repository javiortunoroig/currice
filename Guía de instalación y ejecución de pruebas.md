Para ejecutar el conjunto de pruebas desarrolladas en Cypress, debemos de tener instalado node y npm, se puede descargar desde la [página oficial](https://nodejs.org/es/). Ahora seguiremos los siguientes pasos:

1. Abrir una consola en la carpeta del proyecto
2. Escribir `npx cypress open`
3. Si no tenemos Crypress instalado, nos pedirá permisos para su instalación, solo debemos escribir 'y' cuando nos pregunte en la consola para instalarlo.
4. Una vez finalizada la instalación, se abrirá Cypress con el directorio de pruebas del proyecto.
5. Para ejecutar las pruebas podemos pulsar el nombre de un archivo de pruebas o pulsar en el botón en la parte derecha de la ventana 'Run X integration specs'.
6. Las pruebas empiezan a ejecutarse evaluando los asserts definidos.

Podemos ver la implementación de las pruebas accediendo a los archivos javascript en directorio cypress > integration .