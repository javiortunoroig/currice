# Contribuyendo

Cuando contribuya a este repositorio, primero discuta el cambio que desea realizar a través de un problema, correo electrónico o cualquier otro método con los propietarios de este repositorio antes de realizar un cambio.

Tenga en cuenta que tenemos un código de conducta, sírvase seguirlo en todas sus interacciones con el proyecto.

## Petición Merge Request

¿Qué es una merge request? Toda la información está disponible en la documentación oficial de [GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/). 
Es la manera que implementa gitLab para mejorar el trabajo en equipo, e impulsar las contribuciones a proyectos de código abierto. Cuando termines una funcionalidad que hayas estado implementando en una rama y quieras unirla a la rama principal harás un merge request en vez de un merge. Esta petición será revisada por las personas de mayor rol en el proyecto, los cuales evaluarán tu petición, aceptándola o denegándola. En caso de no aceptarla dirán el motivo por el que se ha rechazado para que se pueda mejorar los cambios hechos y volver a presentarlo en un futuro. Además cualquier persona del proyecto, o ajena al mismo (en proyectos open source) podrán comentar tu merge request ayudando a mejorarlo continuamente.

### Normas para el Merge Request y commits

* Las ramas serán usadas para implementar una única funcionalidad.
* Los nombres de las ramas deberán ser autodescriptivos, por ejemplo, si vas a crear un inicio de sesión podría nombrar a la rama `inicio-sesion`.
* Los commits deberán de ser atómicos. Es decir, deberán de hacerse commits de cambios en el código estrechamente relacionados. Por ejemplo, si creamos un navbar y más tarde se hace formulario, primero se hará un commit con el código de navbar que se ha cambiado y luego otro commit para el formulario.
* Los mensajes de commits tendrán que indicar que tipo de cambio se ha hecho: normalmente se usarán: feature, fix, documentation. En el punto anterior se podría haber escrito: `git commit -m "feature: Navbar creado"`
* Asegúrese de que todas las dependencias de instalación o compilación se eliminen antes de hacer el merge request.
* Actualice README.md si es necesario, esto incluye nuevas variables de entorno, puertos expuestos, ubicaciones de archivos útiles, documentación de código necesaría...
* El título del merge request debe ser autodescriptivo.
* El merge request y cualquier comentario en el mismo debe de seguir el código de conducta.


### ¿Como hacer un Merge Request?

1. Cree una nueva rama a partir de la master.
2. Haga los commits necesarios hasta tener la rama creada con los cambios que desea implementar.
3. Haga un push de la rama a origin.
4. Vaya a la página del proyecto.
5. Verá una opción con la que crear un merge request con los cambios que se han realizado. En caso de que no aparezca la opción podeis hacer un merge manualmente, sidebar > Repository > Branches > Buscar la rama deseada y hacer Merge request.
6. El merge request será revisado por los desarrolladores del proyecto.
7. Si el merge request es aceptado, se hará un merge de la rama creada con la rama principal.
8. Si el merge request es rechazado, los cambios que se han realizado se perderán.

## Código de conducta

### Nuestro compromiso

Con el interés de fomentar un entorno abierto y acogedor, nosotros, como contribuyentes y mantenedores, nos comprometemos a hacer de la participación en nuestro proyecto y nuestra comunidad una experiencia libre de acoso para todos, independientemente de su edad, tamaño corporal, discapacidad, etnia, identidad y expresión de género, nivel de experiencia, nacionalidad, apariencia personal, raza, religión o identidad y orientación sexual.

### Nuestras Normas

Ejemplos de comportamiento que contribuye a crear un ambiente positivo incluyen:

* Usar un lenguaje acogedor e inclusivo.
* Ser respetuoso con los diferentes puntos de vista y experiencias.
* Aceptando con gracia las críticas constructivas.
* Centrarse en lo que es mejor para la comunidad.
* Mostrar empatía hacia otros miembros de la comunidad.

Ejemplos de comportamiento inaceptable por parte de los participantes incluyen:

* El uso de lenguaje o imágenes sexualizadas y atención o insinuaciones sexuales no deseadas.
* Troll, comentarios insultantes / despectivos y ataques personales o políticos.
* Acoso público o privado.
* Publicar información privada de otros, como una dirección física o electrónica, sin permiso explícito.
* Otra conducta que razonablemente podría considerarse inapropiada en un entorno profesional.

### Nuestras responsabilidades

Los encargados del mantenimiento del proyecto son responsables de aclarar los estándares de comportamiento aceptable y se espera que tomen las medidas correctivas adecuadas y justas en respuesta a cualquier caso de comportamiento inaceptable.

Los encargados del mantenimiento del proyecto tienen el derecho y la responsabilidad de eliminar, editar o rechazar comentarios, confirmaciones, códigos, ediciones de wiki, problemas y otras contribuciones que no estén alineadas con este Código de Conducta, o prohibir temporal o permanentemente a cualquier colaborador por otros comportamientos que lo consideran inapropiado, amenazante, ofensivo o dañino.

### Ejecución de las normas

Los casos de comportamiento abusivo, acosador o inaceptable de cualquier otro modo se pueden informar comunicándose con el responsable de recursos humanos a través del correo "davidcastillo200.dc@gmail.com". Todas las quejas serán revisadas e investigadas y resultarán en una respuesta que se considere necesaria y apropiada a las circunstancias. El equipo del proyecto está obligado a mantener la confidencialidad con respecto al informante de un incidente. Se pueden publicar más detalles de las políticas de aplicación específicas por separado.

Los mantenedores del proyecto que no sigan o hagan cumplir el Código de Conducta de buena fe pueden enfrentar repercusiones temporales o permanentes según lo determinen otros miembros del liderazgo del proyecto.

### Attribution
Este código de conducta se basa en [un repositorio github][repositorio] que a su vez está adaptado del [Pacto de colaboradores][homepage] version 1.4, disponible en [http://contributor-covenant.org/version/1/4][version]

[repositorio]: https://gist.github.com/PurpleBooth/b24679402957c63ec426
[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/
